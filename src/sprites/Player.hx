package sprites;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.graphics.frames.FlxAtlasFrames;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.system.FlxSound;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;


class Player extends FlxSprite
{
    public static var WALK_SPEED:Int = 31;

    public var boomboxSnd:FlxSound;
    public var shakeSnd:FlxSound;
    public var slapSnd:FlxSound;
    public var splashSnd:FlxSound;
    public var stepSnd:FlxSound;

    public var action:String;
    public var doingAction:Bool;
    public var canDoAction:Bool;
    public var acting:Bool;
    public var actionSpeed:Float;
    public var awakeness:Float;
    public var asleep:Bool;
    public var sleepRate:Float;
    public var actionTimer:FlxTimer;
    public var actionWord:String;

    public function new(x:Int, y:Int)
    {
        super(x, y);

        boomboxSnd = FlxG.sound.load("assets/sounds/boombox.wav", 0.7, false);
        shakeSnd = FlxG.sound.load("assets/sounds/shake.wav");
        slapSnd = FlxG.sound.load("assets/sounds/slap.wav");
        splashSnd = FlxG.sound.load("assets/sounds/splash.wav", 0.8, false);
        stepSnd = FlxG.sound.load("assets/sounds/step.wav");

        origin = FlxPoint.get(40, 0);

        frames = FlxAtlasFrames.fromTexturePackerJson(
            "assets/images/player.png", "assets/images/player.json");

        setFacingFlip(FlxObject.LEFT, false, false);
        setFacingFlip(FlxObject.RIGHT, true, false);
        animation.addByPrefix("stand", "player_stand", 3);
        animation.addByPrefix("walk", "player_walk", 5);
        animation.addByPrefix("slap", "player_slap", 3, false);
        animation.addByPrefix("headbutt_pillar", "player_headbutt_pillar", 3,
                              false);
        animation.addByPrefix("headbutt_wall", "player_headbutt_wall", 3,
                              false);
        animation.addByPrefix("splash", "player_splash", 4, false);
        animation.addByPrefix("turn_on_boombox", "player_boombox", 3, false);
        animation.addByPrefix("fire", "player_fire", 3, false);
        animation.addByPrefix("fall_asleep", "player_fall_asleep", 4, false);
        animation.addByPrefix("sleep", "player_sleep", 1, true);
        animation.addByPrefix("final", "player_final", 3, false);

        animation.play("stand");

        action = "slap";
        canDoAction = true;
        actionWord = "Slap Yourself";
        doingAction = false;
        acting = false;
        actionSpeed = 0;
        awakeness = 100;
        asleep = false;
        //FIXME SET TO 0.01
        //sleepRate = 0.1;
        sleepRate = 0.01;

        actionTimer = new FlxTimer();
        actionTimer.start(2, function (timer:FlxTimer):Void {
            if (!doingAction)
            {
                canDoAction = true;
            }
        }, 0);
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);

        if (awakeness == 0 && !asleep)
        {
            asleep = true;
            animation.play("fall_asleep");
        }

        if (asleep) {
            velocity.x = 0;
            if (animation.curAnim.name == "fall_asleep" &&
                animation.curAnim.finished)
            {
                animation.play("sleep");
                new FlxTimer().start(3, function (t:FlxTimer) {
                    FlxG.camera.flash(FlxColor.BLACK, 5);
                });
            }
            return;
        }

        awakeness = FlxMath.bound(awakeness - sleepRate, 0, 100);

        switch action
        {
            case "slap":
            actionWord = "Slap Yourself";
            actionSpeed = 0.8;

            case "headbutt_pillar":
            actionWord = "Bang Your Head Against Pillar";
            actionSpeed = 1.1;

            case "headbutt_wall":
            actionWord = "Bang Your Head Against Wall";
            actionSpeed = 1;

            case "splash":
            actionWord = "Splash Cold Water In Your Face";
            actionSpeed = 1.5;

            case "turn_on_boombox":
            actionWord = "Crank 'Dem Tunes!";
            actionSpeed = 2;

            case "fire":
            actionWord = "Breath Hellish Flames";
            actionSpeed = 1;

            case "final":
            actionWord = "Annihilate Your Nemesis!!";
            actionSpeed = 0.5;
        }

        if (FlxG.keys.justPressed.SPACE)
        {
            if (!doingAction)
            {
                doingAction = true;
                velocity.x = 0;
            }
        }

        if (acting)
        {
            if (animation.finished)
            {
                animation.play("stand");
                acting = false;
            }

            switch animation.curAnim.name
            {
                case "slap":
                if (animation.curAnim.curFrame == 1)
                {
                    FlxG.camera.shake(0.02, 0.2);
                    if (!stepSnd.playing)
                    {
                        stepSnd.play();
                    }
                    if (!slapSnd.playing)
                    {
                        slapSnd.play();
                    }
                }

                case "headbutt_pillar", "headbutt_wall":
                if (animation.curAnim.curFrame == 2)
                {
                    FlxG.camera.shake(0.04, 0.5);
                    if (!shakeSnd.playing)
                    {
                        shakeSnd.play();
                    }
                }

                case "splash":
                if (animation.curAnim.curFrame == 2 && !splashSnd.playing)
                {
                    splashSnd.play();
                }

                case "turn_on_boombox":
                if (animation.curAnim.curFrame == 1 && !boomboxSnd.playing)
                {
                    boomboxSnd.play();
                }
            }
        }
        
        if (!doingAction && !acting)
        {
            if (FlxG.keys.pressed.LEFT || FlxG.keys.pressed.A)
            {
                animation.play("walk");
                facing = FlxObject.LEFT;
                if (animation.curAnim.curFrame < 3 ||
                    (animation.curAnim.curFrame > 5 &&
                     (animation.curAnim.curFrame < 9)))
                {
                    velocity.x = -WALK_SPEED;
                }
                else
                {
                    if (velocity.x != 0)
                    {
                        FlxG.camera.shake(0.01, 0.1);
                        stepSnd.play();
                    }
                    velocity.x = 0;
                }
            }
            else if (FlxG.keys.pressed.RIGHT || FlxG.keys.pressed.D)
            {
                animation.play("walk");
                facing = FlxObject.RIGHT;
                if (animation.curAnim.curFrame < 3 ||
                    (animation.curAnim.curFrame > 5 &&
                     (animation.curAnim.curFrame < 9)))
                {
                    velocity.x = WALK_SPEED;
                }
                else
                {
                    if (velocity.x != 0)
                    {
                        FlxG.camera.shake(0.01, 0.1);
                        stepSnd.play();
                    }
                    velocity.x = 0;
                }
            }
            else
            {
                animation.play("stand");
                velocity.x = 0;
            }

            if (x < 10)
            {
                x = 10;
            }
            if (x > 300)
            {
                x = 300;
            }
        }
    }

    public function doAction():Void
    {
        acting = true;
        switch action
        {
            case "slap":
            animation.play("slap", true);
            awakeness = FlxMath.bound(awakeness + 10, 0, 100);

            case "headbutt_pillar":
            animation.play("headbutt_pillar", true);
            awakeness = FlxMath.bound(awakeness + 20, 0, 100);

            case "headbutt_wall":
            animation.play("headbutt_wall", true);
            awakeness = FlxMath.bound(awakeness + 20, 0, 100);

            case "splash":
            animation.play("splash", true);
            awakeness = FlxMath.bound(awakeness + 40, 0, 100);

            case "turn_on_boombox":
            animation.play("turn_on_boombox", true);

            case "fire":
            animation.play("fire", true);

            case "final":
            animation.play("final", true);
        }
    }

    override public function destroy():Void
    {
        super.destroy();
    }
}
