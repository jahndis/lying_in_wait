package;

import flixel.FlxG;
import flixel.FlxGame;
import flixel.FlxState;
import openfl.display.Sprite;
import openfl.Lib;

import states.MenuState;


class Main extends Sprite
{
	var gameWidth:Int = 160;
	var gameHeight:Int = 120;
	var initialState:Class<FlxState> = MenuState;
	var zoom:Float = 1;
	var framerate:Int = 60;
	var skipSplash:Bool = true;
	var startFullscreen:Bool = false;

	public function new()
	{
		super();
		var stageWidth:Int = Lib.current.stage.stageWidth;
		var stageHeight:Int = Lib.current.stage.stageHeight;

		if (zoom == -1)
		{
			var ratioX:Float = stageWidth / gameWidth;
			var ratioY:Float = stageHeight / gameHeight;
			zoom = Math.min(ratioX, ratioY);
			gameWidth = Math.ceil(stageWidth / zoom);
			gameHeight = Math.ceil(stageHeight / zoom);
		}

        /*
        FlxG.sound.muteKeys = null;
        FlxG.sound.volumeUpKeys = null;
        FlxG.sound.volumeDownKeys = null;
        */

		addChild(new FlxGame(gameWidth, gameHeight,
                             initialState, zoom, framerate, framerate,
                             skipSplash, startFullscreen));

#if flash
        FlxG.sound.playMusic("assets/music/music1.mp3", 1, true);
#else
        FlxG.sound.playMusic("assets/music/music1.ogg", 1, true);
#end
	}
}
