package states;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.effects.particles.FlxEmitter;
import flixel.effects.particles.FlxParticle;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.math.FlxRandom;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.tweens.FlxEase;
import flixel.tweens.FlxTween;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;
import flixel.util.helpers.FlxBounds;

import sprites.Player;


using flixel.util.FlxSpriteUtil;


class GameState extends FlxState
{
    public var rand:FlxRandom;

    public var music1:FlxSound;
    public var music2:FlxSound;

    public var fireSnd:FlxSound;
    public var heroDeadSnd:FlxSound;
    public var heroJumpSnd:FlxSound;
    public var heroWalkSnd:FlxSound;
    public var hitSnd:FlxSound;
    public var missSnd:FlxSound;
    public var waterSnd:FlxSound;

    public var background:FlxSprite;
    public var foreground:FlxSprite;
    public var cameraMan:FlxSprite;

    public var player:Player;

    public var door:FlxSprite;
    public var door2:FlxSprite;

    public var lamp1:FlxSprite;
    public var lamp2:FlxSprite;
    public var lampHitbox1:FlxSprite;
    public var lampHitbox2:FlxSprite;
    public var lamp1Power:Float;
    public var lamp2Power:Float;
    public var lamp1Glow:FlxSprite;
    public var lamp2Glow:FlxSprite;
    public var darkness:FlxSprite;

    public var fireEmitter:FlxEmitter;

    public var wallHealthTimer:FlxTimer;
    public var wallHealth:Int;
    public var wall:FlxSprite;
    public var wallHitbox1:FlxSprite;
    public var wallHitbox2:FlxSprite;

    public var boombox:FlxSprite;
    public var boomboxHitbox:FlxSprite;

    public var pillarHealthTimer:FlxTimer;
    public var pillar1Health:Int;
    public var pillar3Health:Int;
    public var pillar1:FlxSprite;
    public var pillar3:FlxSprite;
    public var pillarHitbox1:FlxSprite;
    public var pillarHitbox2:FlxSprite;
    public var pillarHitbox3:FlxSprite;
    public var pillarHitbox4:FlxSprite;

    public var water1:FlxSprite;
    public var water3:FlxSprite;
    public var waterHitbox1:FlxSprite;
    public var waterHitbox3:FlxSprite;

    public var awakeBarText:FlxText;
    public var awakeBarBorder:FlxSprite;
    public var awakeBar:FlxSprite;
    public var actionBarBorder:FlxSprite;
    public var actionBar:FlxSprite;
    public var actionBarMarker:FlxSprite;
    public var actionBarMarkerValue:Float;
    public var actionDirection:Int;
    public var actionText:FlxText;

    public var sleepRateTimer:FlxTimer;

    public var hero:FlxSprite;
    public var heroState:Int;
    public var heroText:FlxText;
    public var heroTimer:FlxTimer;
    public var heroFloor:Int;

    public var lostGame:Bool;
    public var wonGame:Bool;

	override public function create():Void
	{
		super.create();

        FlxG.mouse.visible = false;

        rand = new FlxRandom();

#if flash
        music2 = FlxG.sound.load("assets/music/music2.mp3", 0, true);
#else
        music2 = FlxG.sound.load("assets/music/music2.ogg", 0, true);
#end
        music2.play();

        fireSnd = FlxG.sound.load("assets/sounds/fire.wav");
        heroDeadSnd = FlxG.sound.load("assets/sounds/hero_dead.wav");
        heroJumpSnd = FlxG.sound.load("assets/sounds/hero_jump.wav", 0.5);
        heroWalkSnd = FlxG.sound.load("assets/sounds/hero_walk.wav");
        hitSnd = FlxG.sound.load("assets/sounds/hit.wav");
        missSnd = FlxG.sound.load("assets/sounds/miss.wav");
        waterSnd = FlxG.sound.load("assets/sounds/water.wav", 0.2, true);
        missSnd.volume = 0.5;
        
        background = new FlxSprite(0, 0);
        background.loadGraphic("assets/images/background.png");
        add(background);

        door = new FlxSprite(5, 74);
        door.loadGraphic("assets/images/door.png", true, 20, 34, true);
        door.animation.add("door", [0, 1, 2, 3, 4, 5, 6, 7], 8, false);
        add(door);
        door.kill();

        door2 = new FlxSprite(355, 74);
        door2.loadGraphic("assets/images/door.png", true, 20, 34, true);
        door2.animation.add("door", [0, 1, 2, 3, 4, 5, 6, 7], 8, false);
        door2.scale.x = -1;
        add(door2);
        door2.kill();

        lamp1Power = 50;
        lamp2Power = 50;

        lamp1 = new FlxSprite(114, 12);
        lamp1.loadGraphic("assets/images/lamp.png", true, 22, 28, true);
        lamp1.animation.add("lamp", [0, 1, 2, 3, 4], 8);
        lamp1.animation.play("lamp");
        add(lamp1);

        lamp2 = new FlxSprite(247, 12);
        lamp2.loadGraphic("assets/images/lamp.png", true, 22, 28, true);
        lamp2.animation.add("lamp", [0, 1, 2, 3, 4], 8);
        lamp2.animation.play("lamp");
        add(lamp2);

        lampHitbox1 = new FlxSprite(55, 75);
        lampHitbox1.makeGraphic(1, 1, 0x0);
        add(lampHitbox1);

        lampHitbox2 = new FlxSprite(186, 75);
        lampHitbox2.makeGraphic(1, 1, 0x0);
        add(lampHitbox2);

        wall = new FlxSprite(95, 64);
        wall.loadGraphic("assets/images/wall.png", true, 58, 44, true);
        add(wall);

        wallHitbox1 = new FlxSprite(124, 75);
        wallHitbox1.makeGraphic(1, 1, 0x0);
        add(wallHitbox1);

        wallHitbox2 = new FlxSprite(257, 75);
        wallHitbox2.makeGraphic(1, 1, 0x0);
        add(wallHitbox2);

        //FIXME CHANGE BACK TO 5!!
        //wallHealth = 1;
        wallHealth = 5;

        wallHealthTimer = new FlxTimer();

        boombox = new FlxSprite(113, 82);
        boombox.loadGraphic("assets/images/boombox.png", true, 24, 14, true);
        boombox.animation.add("boombox", [0, 1, 2, 3, 4, 5], 5);
        add(boombox);
        boombox.kill();

        boomboxHitbox = new FlxSprite(125, 75);
        boomboxHitbox.makeGraphic(1, 1, 0x0);
        add(boomboxHitbox);
        boomboxHitbox.kill();

        cameraMan = new FlxSprite(0, FlxG.height / 2);
        cameraMan.makeGraphic(1, 1, 0x0);
        add(cameraMan);

        fireEmitter = new FlxEmitter(0, 0, 1000);
        fireEmitter.makeParticles(3, 3, 0xFFFFFF00, 1000);
        fireEmitter.launchMode = FlxEmitterMode.CIRCLE;
        fireEmitter.lifespan.set(0.5, 1);
        fireEmitter.scale.set(1, 1, 2, 2, 0.5, 0.5);
        fireEmitter.color.set(FlxColor.YELLOW, FlxColor.ORANGE, FlxColor.RED,
                              FlxColor.YELLOW);
        fireEmitter.angularVelocity.set(300, 700);
        fireEmitter.speed.set(100, 200);
        fireEmitter.drag.set(100);
        fireEmitter.alpha.set(1, 0);
        add(fireEmitter);
            
        //FIXME CHANGE X VAL TO 160
        player = new Player(160, 40);
        add(player);

        hero = new FlxSprite(16, 90);
        hero.loadGraphic("assets/images/hero.png", true, 16, 20, true);
        hero.animation.add("run", [0, 1, 2, 3], 5, true);
        hero.animation.add("stand", [4], 1, false);
        hero.animation.add("uh", [5, 6, 7, 8, 9], 1, false);
        hero.animation.add("oh", [10, 10, 10, 11, 11], 1, false);
        hero.animation.add("jump", [12, 13], 5, true);
        hero.animation.add("fight", [14, 15, 16, 1, 16, 15], 5, true);
        hero.animation.add("dead", [17, 18, 19], 4, true);
        add(hero);
        hero.kill();

        FlxG.camera.setScrollBounds(0, 380, 0, 120);
        FlxG.camera.follow(cameraMan, FlxCameraFollowStyle.LOCKON, 0.5);

        water1 = new FlxSprite(59, 35);
        water1.loadGraphic("assets/images/water.png", true, 66, 80, true);
        water1.animation.add("water", [0, 1, 2], 8);
        water1.animation.play("water");
        add(water1);
        water1.kill();

        water3 = new FlxSprite(191, 35);
        water3.loadGraphic("assets/images/water.png", true, 66, 80, true);
        water3.animation.add("water", [0, 1, 2], 8);
        water3.animation.play("water");
        add(water3);
        water3.kill();

        waterHitbox1 = new FlxSprite(92, 75);
        waterHitbox1.makeGraphic(1, 1, 0x0);
        add(waterHitbox1);
        waterHitbox1.kill();

        waterHitbox3 = new FlxSprite(224, 75);
        waterHitbox3.makeGraphic(1, 1, 0x0);
        add(waterHitbox3);
        waterHitbox3.kill();

        foreground = new FlxSprite(0, 0);
        foreground.loadGraphic("assets/images/foreground.png");
        add(foreground);

        pillar1 = new FlxSprite(59, 35);
        pillar1.loadGraphic("assets/images/pillar.png", true, 66, 80, true);
        add(pillar1);

        pillar3 = new FlxSprite(191, 35);
        pillar3.loadGraphic("assets/images/pillar.png", true, 66, 80, true);
        add(pillar3);

        pillarHitbox1 = new FlxSprite(92, 75);
        pillarHitbox1.makeGraphic(1, 1, 0x0);
        add(pillarHitbox1);

        pillarHitbox2 = new FlxSprite(158, 75);
        pillarHitbox2.makeGraphic(1, 1, 0x0);
        add(pillarHitbox2);

        pillarHitbox3 = new FlxSprite(224, 75);
        pillarHitbox3.makeGraphic(1, 1, 0x0);
        add(pillarHitbox3);

        pillarHitbox4 = new FlxSprite(290, 75);
        pillarHitbox4.makeGraphic(1, 1, 0x0);
        add(pillarHitbox4);

        pillarHealthTimer = new FlxTimer();

        //FIXME CHANGE THIS BACK TO 3!!
        //pillar1Health = 1;
        //pillar3Health = 1;
        pillar1Health = 3;
        pillar3Health = 3;

        darkness = new FlxSprite(0, 0);
        darkness.makeGraphic(FlxG.width, FlxG.height, 0xFF000000);
        darkness.alpha = 0;
        darkness.scrollFactor.set();
        add(darkness);

        lamp1Glow = new FlxSprite(114, 14);
        lamp1Glow.makeGraphic(22, 22, 0x0);
        lamp1Glow.drawCircle(-1, -1, -1, 0x22FFFFDD);
        add(lamp1Glow);

        lamp2Glow = new FlxSprite(247, 14);
        lamp2Glow.makeGraphic(22, 22, 0x0);
        lamp2Glow.drawCircle(-1, -1, -1, 0x22FFFFDD);
        add(lamp2Glow);

        awakeBarText = new FlxText((FlxG.width / 2) - 12, 2, 0, "Zzz");
        awakeBarText.color = 0xFFC0B0D4;
        awakeBarText.borderSize = 1;
        awakeBarText.borderColor = 0xFF201826;
        awakeBarText.borderStyle = FlxTextBorderStyle.OUTLINE;
        awakeBarText.scrollFactor.set();
        add(awakeBarText);

        awakeBarBorder = new FlxSprite(5, 15);
        awakeBarBorder.makeGraphic(FlxG.width - 10, 6, 0xFF000000);
        awakeBarBorder.scrollFactor.set();
        add(awakeBarBorder);

        awakeBar= new FlxSprite(6, 16);
        awakeBar.makeGraphic(FlxG.width - 12, 4, 0xFF544066);
        awakeBar.scrollFactor.set();
        add(awakeBar);

        actionBarBorder = new FlxSprite(5, FlxG.height - 5 - 4);
        actionBarBorder.makeGraphic(FlxG.width - 10, 4, 0xFF000000);
        actionBarBorder.scrollFactor.set();
        add(actionBarBorder);

        actionBar = new FlxSprite(6, FlxG.height - 4 - 4);
        actionBar.makeGraphic(FlxG.width - 12, 2, 0xFFECE766);
        actionBar.scrollFactor.set();
        add(actionBar);

        actionBarMarker = new FlxSprite(6, FlxG.height - 7 - 4);
#if flash
        actionBarMarker.makeGraphic(8, 8, 0x0);
        actionBarMarker.drawLine(0, 0, 0, 7, {color: 0xFFFF0000});
        actionBarMarker.drawLine(0, 0, 7, 0, {color: 0xFFFF0000});
        actionBarMarker.drawLine(7, 0, 7, 8, {color: 0xFFFF0000});
        actionBarMarker.drawLine(0, 7, 7, 7, {color: 0xFFFF0000});
#else
        actionBarMarker.makeGraphic(4, 8, 0x0);
        actionBarMarker.drawLine(0, 0, 0, 7, {color: 0xFFFF0000});
        actionBarMarker.drawLine(0, 0, 3, 0, {color: 0xFFFF0000});
        actionBarMarker.drawLine(3, 0, 3, 8, {color: 0xFFFF0000});
        actionBarMarker.drawLine(0, 7, 3, 7, {color: 0xFFFF0000});
#end
        actionBarMarker.scrollFactor.set();
        add(actionBarMarker);
        actionBarMarkerValue = 0;

        actionDirection = 1;

        actionText = new FlxText(0, 20, 0, "blah");
        actionText.x = (FlxG.width / 2) - (actionText.fieldWidth / 2);
        actionText.color = 0xFFebea9d;
        actionText.scrollFactor.set();
        add(actionText);

        heroText = new FlxText(0, FlxG.height / 2, 0, "");
        heroText.scrollFactor.set();
        heroText.color = 0xFFDD0000;
        heroText.borderSize = 2;
        heroText.borderStyle = FlxTextBorderStyle.OUTLINE;
        heroText.size = 12;
        heroText.alignment = FlxTextAlign.CENTER;
        heroText.x = (FlxG.width / 2) - (heroText.fieldWidth / 2);
        add(heroText);

        heroFloor = 1;
        heroState = -1;

        heroTimer = new FlxTimer();
        //FIXME CHANGE TIMER TO 60 SECONDS
        //heroTimer.start(5, function (t:FlxTimer) {
        heroTimer.start(60, function (t:FlxTimer) {
            switch heroFloor
            {
                case 1:
                heroText.text = "Hero Completed\n1st Floor!";

                case 2:
                heroText.text = "Hero Completed\n2nd Floor!";

                case 3:
                heroText.text = "Hero Completed\n3rd Floor!";

                case 4:
                heroText.text = "Hero Completed\n4th Floor!";

                case 5:
                heroText.text = "Hero Completed\n5th Floor!";
                t.cancel();
                wonGame = true;
            }
            heroText.x = -500;
            FlxTween.tween(
                heroText,
                { x: (FlxG.width / 2) - (heroText.fieldWidth / 2) },
                2,
                {
                    ease: FlxEase.quadInOut,
                    onComplete: function (t:FlxTween) {
                        FlxTween.tween(
                            heroText,
                            { x: (FlxG.width + 500) },
                            1,
                            { ease: FlxEase.quadInOut, startDelay: 1 });
                    }
                });

            heroFloor += 1;
        }, 5);

        sleepRateTimer = new FlxTimer();
        sleepRateTimer.start(1, function (t:FlxTimer) {
            if (boombox.exists && boombox.animation.name == "boombox")
            {
                player.sleepRate += (0.00001 * (
                    100 - (lamp1Power + lamp2Power) + 1)) * 0.5;
            }
            else
            {
                player.sleepRate += 0.00001 * (
                    100 - (lamp1Power + lamp2Power) + 1);
            }
        }, 0);

        FlxG.camera.fade(FlxColor.BLACK, 1, true);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);

        if (boombox.exists && boombox.animation.name == "boombox" &&
            !(lostGame || wonGame))
        {
            music2.volume = 1;
            FlxG.sound.music.volume = 0;
        }

        if (water1.exists || water3.exists)
        {
            waterSnd.play();
        }

        if (lostGame || wonGame)
        {
            heroTimer.cancel();
            FlxG.sound.music.volume = FlxMath.bound(
                FlxG.sound.music.volume + 0.1, 0, 1);
            music2.volume = FlxMath.bound(music2.volume - 0.1, 0, 1);
            darkness.alpha = 0;
            lamp1Power = 50;
            lamp2Power = 50;

            if (wonGame)
            {
                player.awakeness = 100;
            }

            player.velocity.x = 0;
            if (wonGame && heroState < 0)
            {
                actionText.visible = false;
                if (player.animation.name != "final" && 
                    player.animation.name != "fall_asleep" &&
                    player.animation.name != "sleep")
                {
                    player.animation.play("stand");
                }
                player.facing = FlxObject.LEFT;
                heroState = 0;
            }

            if (lostGame && heroState < 0)
            {
                actionText.visible = false;
                new FlxTimer().start(7, function (t:FlxTimer) {
                    heroState = 0;
                });
            }

            actionBar.visible = false;
            actionBarBorder.visible = false;
            actionBarMarker.visible = false;
            awakeBar.visible = false;
            awakeBarBorder.visible = false;
            awakeBarText.visible = false;

            switch (heroState)
            {
                case 0:
                if (!door.exists)
                {
                    door.revive();
                    door.animation.play("door");
                    door2.revive();
                    door2.animation.play("door");
                    hero.revive();
                    hero.animation.play("run");
                    hero.velocity.x = 10;
                }

                if (hero.x >= player.x - 30 &&
                    (hero.x < 70 || hero.x > 100) &&
                    (hero.x < 135 || hero.x > 166) &&
                    (hero.x < 201 || hero.x > 232) &&
                    (hero.x < 270 || hero.x > 298))
                {
                    heroState = 1;
                }
                else
                {
                    heroWalkSnd.play();
                }

                case 1:
                hero.velocity.x = 0;
                hero.animation.play("stand");
                new FlxTimer().start(1, function (t:FlxTimer) {
                    heroState = 2;
                });

                case 2:
                if (lostGame)
                {
                    if (hero.animation.name == "stand")
                    {
                        hero.animation.play("uh");
                    }
                    if (hero.animation.finished)
                    {
                        heroState = 3;
                    }
                }
                if (wonGame)
                {
                    player.facing = FlxObject.LEFT;
                    if (hero.animation.name == "stand")
                    {
                        hero.animation.play("oh");
                    }
                    new FlxTimer().start(4, function (t:FlxTimer) {
                        heroState = 5;
                    });
                }

                case 3:
                hero.animation.play("jump");
                heroJumpSnd.play();
                new FlxTimer().start(3, function (t:FlxTimer) {
                    heroState = 4;
                });

                case 4:
                hero.animation.play("run");
                hero.velocity.x = 10;
                heroWalkSnd.play();
                if (hero.x > 364)
                {
                    hero.x += 1000;
                }
                new FlxTimer().start(5, function (t:FlxTimer) {
                    FlxG.camera.fade(FlxColor.BLACK, 1, false,
                        function () {
                            FlxG.switchState(new LoseState());
                        });
                });

                case 5:
                if (hero.animation.name == "oh")
                {
                    hero.animation.play("fight");
                    heroWalkSnd.play();
                    player.action = "final";
                    player.actionWord = "Annihilate Your Nemesis!!";
                    actionText.text = player.actionWord;
                    actionText.x = (FlxG.width/2) - (actionText.fieldWidth/2);
                    actionText.visible = true;
                }

                if (FlxG.keys.justPressed.SPACE && actionText.visible)
                {
                    player.doAction();
                    actionText.visible = false;
                    wallHealthTimer.cancel();
                    wallHealthTimer.start(0.8, function (t:FlxTimer) {
                        generateFire();
                        hero.animation.play("dead");
                        heroDeadSnd.play();
                    });

                    new FlxTimer().start(3, function (t:FlxTimer) {
                        player.animation.play("fall_asleep");
                        new FlxTimer().start(2, function (t:FlxTimer) {
                            player.animation.play("sleep");
                            new FlxTimer().start(3, function (t:FlxTimer) {
                                FlxG.camera.fade(FlxColor.BLACK, 1, false,
                                    function () {
                                        FlxG.switchState(new WinState());
                                    });
                            });
                        });
                    });
                }
            }

            return;
        }

        darkness.alpha = FlxMath.bound(
            (100 - (lamp1Power + lamp2Power)) / 100, 0, 0.7);
        lamp1Power = FlxMath.bound(lamp1Power - 0.01, 0, 50);
        lamp2Power = FlxMath.bound(lamp2Power - 0.01, 0, 50);

        lamp1Glow.scale.x = (lamp1Power / 50) + rand.float(0, 0.1);
        lamp1Glow.scale.y = lamp1Glow.scale.x;
        lamp2Glow.scale.x = (lamp2Power / 50) + rand.float(0, 0.1);
        lamp2Glow.scale.y = lamp2Glow.scale.x;

        if (player.facing == FlxObject.LEFT)
        {
            lampHitbox1.x = 196;
            lampHitbox2.x = 328;
        }
        else
        {
            lampHitbox1.x = 55;
            lampHitbox2.x = 186;
        }

        if (boombox.exists && boombox.animation.name == "boombox")
        {
            boombox.scale.x = rand.float(1, 1.2);
            boombox.scale.y = rand.float(1, 1.2);
            cameraMan.x = player.x + (player.width / 2);
            cameraMan.x += rand.int(-1, 1);
        }
        else
        {
            cameraMan.x = player.x + (player.width / 2);
        }

        awakeBar.scale.x = FlxMath.bound(player.awakeness / 100, 0, 1);

        if (player.awakeness == 0)
        {
            lostGame = true;
        }

        if ((Math.abs((player.x + 35) - (pillarHitbox1.x)) < 5 &&
             pillarHitbox1.exists) ||
            (Math.abs((player.x + 35) - (pillarHitbox2.x)) < 5 &&
             pillarHitbox2.exists) ||
            (Math.abs((player.x + 35) - (pillarHitbox3.x)) < 5 &&
             pillarHitbox3.exists) ||
            (Math.abs((player.x + 35) - (pillarHitbox4.x)) < 5 &&
             pillarHitbox4.exists))
        {
            player.action = "headbutt_pillar";
        }
        else if ((Math.abs((player.x + 35) - (wallHitbox1.x)) < 5 &&
                 wallHitbox1.exists) ||
                 (Math.abs((player.x + 35) - (wallHitbox2.x)) < 5 &&
                 wallHitbox2.exists))
        {
            player.action = "headbutt_wall";
        }
        else if ((Math.abs((player.x + 35) - (boomboxHitbox.x)) < 5 &&
                 boomboxHitbox.exists))
        {
            player.action = "turn_on_boombox";
        }
        else if ((Math.abs((player.x + 35) - (waterHitbox1.x)) < 5 &&
                  waterHitbox1.exists) ||
                 (Math.abs((player.x + 35) - (waterHitbox3.x)) < 5 &&
                  waterHitbox3.exists))
        {
            player.action = "splash";
        }
        else if ((Math.abs((player.x + 35) - (lampHitbox1.x)) < 5 &&
                  lampHitbox1.exists) ||
                 (Math.abs((player.x + 35) - (lampHitbox2.x)) < 5 &&
                  lampHitbox2.exists))
        {
            player.action = "fire";
        }
        else
        {
            player.action = "slap";
        }
        
        if (!player.acting && !player.canDoAction)
        {
            player.doingAction = false;
        }

        if (player.doingAction)
        {
            if (FlxG.keys.justPressed.SPACE && actionBar.visible &&
                player.canDoAction)
            {
                player.canDoAction = false;

#if flash
                if (Math.abs((actionBar.scale.x * 100) - actionBarMarkerValue)
                    < 8)
#else
                if (Math.abs((actionBar.scale.x * 100) - actionBarMarkerValue)
                    < 3)
#end
                {
                    hitSnd.play();
                    player.doAction();
                    if (player.canDoAction)
                    {
                        actionBarMarkerValue = rand.int(0, 100);
                    }
                    else
                    {
                        player.doingAction = false;
                    }
                    switch player.action
                    {
                        case "headbutt_pillar":
                        if (Math.abs((player.x + 35) - (pillarHitbox1.x)) < 5)
                        {
                            pillarHealthTimer.start(0.8, function (t:FlxTimer) {
                                pillar1Health -= 1;
                                if (pillar1Health > 0)
                                {
                                    pillar1.frame = pillar1.frames.getByIndex(
                                        Std.int(3 - pillar1Health));
                                }
                                else
                                {
                                    pillar1.destroy();
                                    pillarHitbox1.destroy();
                                    water1.revive();
                                    waterHitbox1.revive();
                                    player.doingAction = false;
                                }
                            });
                        }
                        if (Math.abs((player.x + 35) - (pillarHitbox3.x)) < 5)
                        {
                            pillarHealthTimer.start(0.8, function (t:FlxTimer) {
                                pillar3Health -= 1;
                                if (pillar3Health > 0)
                                {
                                    pillar3.frame = pillar3.frames.getByIndex(
                                        Std.int(3 - pillar3Health));
                                }
                                else
                                {
                                    pillar3.destroy();
                                    pillarHitbox3.destroy();
                                    water3.revive();
                                    waterHitbox3.revive();
                                    player.doingAction = false;
                                }
                            });
                        }

                        case "headbutt_wall":
                        if (Math.abs((player.x + 35) - (wallHitbox1.x)) < 5)
                        {
                            wallHealthTimer.start(0.8, function (t:FlxTimer) {
                                wallHealth -= 1;
                                if (wallHealth > 0)
                                {
                                    wall.frame = wall.frames.getByIndex(
                                        Std.int((5 - wallHealth) / 2));
                                }
                                else
                                {
                                    wall.destroy();
                                    wallHitbox1.destroy();
                                    boombox.revive();
                                    boomboxHitbox.revive();
                                    player.doingAction = false;
                                }
                            });
                        }

                        case "turn_on_boombox":
                        if (Math.abs((player.x + 35) - (boomboxHitbox.x)) < 5)
                        {
                            wallHealthTimer.start(0.8, function (t:FlxTimer) {
                                boombox.animation.play("boombox");
                                boomboxHitbox.destroy();
                                player.doingAction = false;
                            });
                        }

                        case "fire":
                        if (Math.abs((player.x + 35) - (lampHitbox1.x)) < 5)
                        {
                            wallHealthTimer.start(0.8, function (t:FlxTimer) {
                                lamp1Power = 50;
                                generateFire();
                                player.doingAction = false;
                            });
                        }
                        if (Math.abs((player.x + 35) - (lampHitbox2.x)) < 5)
                        {
                            wallHealthTimer.start(0.8, function (t:FlxTimer) {
                                lamp2Power = 50;
                                generateFire();
                                player.doingAction = false;
                            });
                        }
                        
                    }
                }
                else
                {
                    missSnd.play();
                    //MAYBE LEAVE THIS OUT?
                    //player.doingAction = false;
                }
            }

            if (!actionBar.visible)
            {
                actionBarBorder.visible = true;
                actionBar.visible = true;
                actionBarMarker.visible = true;
                actionBar.scale.x = 0;
                actionDirection = 1;
                actionBarMarkerValue = rand.int(0, 100);
            }

            actionBar.scale.x += player.actionSpeed * actionDirection / 100;
            actionBar.updateHitbox();
            if (actionDirection == 1)
            {
                if (actionBar.scale.x >= 1)
                {
                    actionDirection = -1;
                }
            }
            else
            {
                if (actionBar.scale.x <= 0)
                {
                    actionDirection = 1;
                }
            }
            actionBarMarker.x = 7 + ((actionBarMarkerValue / 100) *
                                     (FlxG.width - 14)) -
                                     actionBarMarker.width / 2;
        }
        else
        {
            actionBarBorder.visible = false;
            actionBar.visible = false;
            actionBarMarker.visible = false;
        }

        actionText.text = player.actionWord;
        actionText.x = (FlxG.width / 2) - (actionText.fieldWidth / 2);
        actionText.visible = player.canDoAction;

        //FIXME TAKE THIS OUT!!
        //lostGame = true;
        //wonGame = true;
	}

    public function generateFire():Void
    {
        if (wonGame)
        {
            fireEmitter.x = player.x + 7;
            fireEmitter.y = player.y + 45;
            fireEmitter.launchAngle.set(105, 155);
        }
        else
        {
            if (player.facing == FlxObject.LEFT)
            {
                fireEmitter.x = player.x;
                fireEmitter.y = player.y + 24;
                fireEmitter.launchAngle.set(-120, -150);
            }
            else
            {
                fireEmitter.x = player.x + player.width;
                fireEmitter.y = player.y + 24;
                fireEmitter.launchAngle.set(-30, -60);
            }
        }
        fireEmitter.start(false, 0.001, 450);
        fireSnd.play();
    }

	override public function destroy():Void
	{
		super.destroy();
	}
}
