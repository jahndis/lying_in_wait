package states;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxColor;

import sprites.Player;


class IntroState extends FlxState
{
    public var sky:FlxSprite;
    public var text:FlxText;
    public var text2:FlxText;
    public var player:Player;
    public var introState:Int;
    public var controls:FlxText;

    override public function create():Void
    {
        super.create();

        FlxG.mouse.visible = false;

        introState = 0;

        sky = new FlxSprite(0, 0);
        sky.loadGraphic("assets/images/sky.png");
        add(sky);

        text = new FlxText(10, 20, 0, "");
        text.alignment = FlxTextAlign.CENTER;
        text.color = 0xFFebea9d;
        text.x = (FlxG.width / 2) - (text.fieldWidth / 2);
        add(text);

        text2 = new FlxText(10, 20, 0, "");
        text2.alignment = FlxTextAlign.CENTER;
        text2.color = 0xFFebea9d;
        text2.x = (FlxG.width / 2) - (text2.fieldWidth / 2);
        add(text2);

        player = new Player(45, 25);
        player.awakeness = 0;
        add(player);

        controls = new FlxText(10, FlxG.height - 20, 0, "Space");
        controls.alignment = FlxTextAlign.CENTER;
        controls.color = 0xFFc39b9b;
        controls.x = (FlxG.width / 2) - (controls.fieldWidth / 2);
        add(controls);

        FlxG.camera.fade(FlxColor.BLACK, 1, true);
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);

        player.animation.play("sleep");
        player.facing = FlxObject.LEFT;
        player.velocity.x = 0;

        if (FlxG.keys.justPressed.SPACE)
        {
            switch introState
            {
                case 0:
                text.text = "Hey...";
                text2.text = "";

                case 1:
                text.text = "Hey!";
                text2.text = "";

                case 2:
                text.text = "HEY YOU STUPID MONSTER!";
                text2.text = "";

                case 3:
                text.text = "WAKE UUUUUPPPP!!!";
                text2.text = "";

                case 4:
                text.text = "The hero only needs to\n" +
                            "get through five floors\n" +
                            "before he gets to you!!";
                text2.text = "";

                case 5:
                text.text = "On top of that, the minions\n" +
                            "haven't shown themselves to be";
                text2.text = "even slightly competent, so...";
                text2.y = 40;

                case 6:
                text.text = "ARE YOU EVEN LISTENING??!!";
                text2.text = "";

                case 7:
                text.text = "Fine. Have it your way...";
                text2.text = "";

                case 8:
                text.text = "Just don't blame me when\n" +
                            "the hero just waltzes through";
                text2.text = "and defeats our dungeon!";
                text2.y = 40;

                case 9:
                text.text = "...";
                text2.text = "";
                
                case 10:
                text.text = "You really don't care do you?!";
                text2.text = "";

                case 11:
                text.text = "*sigh*";
                text2.text = "";

                case 12:
                text.text = "Look, just try and stay awake\n" +
                            "so we don't look completely\n" +
                            "incompetent...";
                text2.text = "";

                case 13:
                text.text = "Do whatever you feel is";
                text2.text = "necessary to STAY AWAKE!!";
                text2.y = 30;

                case 14:
                text.text = "Stupid monster...";
                text2.text = "";

                case 15:
                FlxG.camera.fade(FlxColor.BLACK, 1, false, function () {
                    FlxG.switchState(new GameState());
                });
            }
            introState += 1;
            text.x = (FlxG.width / 2) - (text.fieldWidth / 2);
            text2.x = (FlxG.width / 2) - (text2.fieldWidth / 2);
        }
    }

    override public function destroy():Void
    {
        super.destroy();
    }
}
