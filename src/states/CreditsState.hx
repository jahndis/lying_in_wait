package states;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;

class CreditsState extends FlxState
{
    public var sky:FlxSprite;
    public var credits:FlxText;
    public var credits2:FlxText;
    public var credits3:FlxText;
    public var credits4:FlxText;
    public var credits5:FlxText;
    public var credits6:FlxText;
    public var credits7:FlxText;
    public var credits8:FlxText;
    public var credits9:FlxText;
    public var credits10:FlxText;
    public var go:Bool;

    override public function create():Void
    {
        super.create();

        FlxG.mouse.visible = false;

        sky = new FlxSprite(0, 0);
        sky.loadGraphic("assets/images/sky.png");
        add(sky);

        credits = new FlxText(10, FlxG.height / 2, 0, "Lying In Wait");
        credits2 = new FlxText(0, FlxG.height, 0, "Created by John Langewisch");
        credits3 = new FlxText(0, FlxG.height + 10, 0, "(whalebot)");
        credits4 = new FlxText(0, FlxG.height + 40, 0, "Follow me on twitter!");
        credits5 = new FlxText(0, FlxG.height + 50, 0, "@jahndis");
        credits6 = new FlxText(0, FlxG.height + 70, 0,
            "Check out my other games!\n" +
            "whalebot.itchio.com");
        credits7 = new FlxText(0, FlxG.height + 100, 0,
            "This game was made for the");
        credits8 = new FlxText(0, FlxG.height + 110, 0,
            "33rd Ludum Dare 48 hour");
        credits9 = new FlxText(0, FlxG.height + 120, 0, "competition.");
        credits10 = new FlxText(0, FlxG.height + 150, 0,
            "Thanks for playing!!");
        credits.alignment = FlxTextAlign.CENTER;
        credits.color = 0xFFebea9d;
        credits.x = (FlxG.width / 2) - (credits.fieldWidth / 2);
        add(credits);
        credits2.alignment = FlxTextAlign.CENTER;
        credits2.color = 0xFFebea9d;
        credits2.x = (FlxG.width / 2) - (credits2.fieldWidth / 2);
        add(credits2);
        credits3.alignment = FlxTextAlign.CENTER;
        credits3.color = 0xFFebea9d;
        credits3.x = (FlxG.width / 2) - (credits3.fieldWidth / 2);
        add(credits3);
        credits4.alignment = FlxTextAlign.CENTER;
        credits4.color = 0xFFebea9d;
        credits4.x = (FlxG.width / 2) - (credits4.fieldWidth / 2);
        add(credits4);
        credits5.alignment = FlxTextAlign.CENTER;
        credits5.color = 0xFFebea9d;
        credits5.x = (FlxG.width / 2) - (credits5.fieldWidth / 2);
        add(credits5);
        credits6.alignment = FlxTextAlign.CENTER;
        credits6.color = 0xFFebea9d;
        credits6.x = (FlxG.width / 2) - (credits6.fieldWidth / 2);
        add(credits6);
        credits7.alignment = FlxTextAlign.CENTER;
        credits7.color = 0xFFebea9d;
        credits7.x = (FlxG.width / 2) - (credits7.fieldWidth / 2);
        add(credits7);
        credits8.alignment = FlxTextAlign.CENTER;
        credits8.color = 0xFFebea9d;
        credits8.x = (FlxG.width / 2) - (credits8.fieldWidth / 2);
        add(credits8);
        credits9.alignment = FlxTextAlign.CENTER;
        credits9.color = 0xFFebea9d;
        credits9.x = (FlxG.width / 2) - (credits9.fieldWidth / 2);
        add(credits9);
        credits10.alignment = FlxTextAlign.CENTER;
        credits10.color = 0xFFebea9d;
        credits10.x = (FlxG.width / 2) - (credits10.fieldWidth / 2);
        add(credits10);

        FlxG.camera.fade(FlxColor.BLACK, 1, true);
        go = false;
        new FlxTimer().start(2, function (t:FlxTimer) { go = true; });
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);

        if (go)
        {
            credits.y -= 0.2;
            credits2.y -= 0.2;
            credits3.y -= 0.2;
            credits4.y -= 0.2;
            credits5.y -= 0.2;
            credits6.y -= 0.2;
            credits7.y -= 0.2;
            credits8.y -= 0.2;
            credits9.y -= 0.2;
            credits10.y -= 0.2;

            if ((FlxG.keys.justPressed.SPACE && credits.y < -20) ||
                credits.y < -200)
            {
                FlxG.camera.fade(FlxColor.BLACK, 1, false, function () {
                    FlxG.switchState(new MenuState());
                });
            }
        }
    }

    override public function destroy():Void
    {
        super.destroy();
    }
}
