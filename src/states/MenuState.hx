package states;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxColor;

import sprites.Player;


class MenuState extends FlxState
{
    public var sky:FlxSprite;
    public var title:FlxText;
    public var start:FlxText;
    public var player:Player;
    public var controls:FlxText;
    public var controls2:FlxText;

    override public function create():Void
    {
        super.create();

        FlxG.mouse.visible = false;

        sky = new FlxSprite(0, 0);
        sky.loadGraphic("assets/images/sky.png");
        add(sky);

        title = new FlxText(10, 10, 0, "Lying In Wait");
        title.color = 0xFFDD0000;
        title.borderSize = 2;
        title.borderStyle = FlxTextBorderStyle.OUTLINE;
        title.size = 18;
        title.alignment = FlxTextAlign.CENTER;
        title.x = (FlxG.width / 2) - (title.fieldWidth / 2);
        add(title);

        start = new FlxText(10, 35, 0, "Space to Start");
        start.alignment = FlxTextAlign.CENTER;
        start.color = 0xFFebea9d;
        start.x = (FlxG.width / 2) - (start.fieldWidth / 2);
        add(start);

        player = new Player(45, 25);
        player.awakeness = 0;
        add(player);

        controls = new FlxText(10, FlxG.height - 25, 0, "Move: Arrows or A/D");
        controls2 = new FlxText(10, FlxG.height - 15, 0, "Do Action: Space");
                  
        controls.alignment = FlxTextAlign.CENTER;
        controls.color = 0xFFc39b9b;
        controls.x = (FlxG.width / 2) - (controls.fieldWidth / 2);
        add(controls);
        controls2.alignment = FlxTextAlign.CENTER;
        controls2.color = 0xFFc39b9b;
        controls2.x = (FlxG.width / 2) - (controls2.fieldWidth / 2);
        add(controls2);

        FlxG.camera.fade(FlxColor.BLACK, 1, true);
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);

        player.animation.play("sleep");
        player.facing = FlxObject.LEFT;
        player.velocity.x = 0;

        if (FlxG.keys.justPressed.SPACE)
        {
            FlxG.camera.fade(FlxColor.BLACK, 1, false, function () {
                FlxG.switchState(new IntroState());
            });
        }
    }

    override public function destroy():Void
    {
        super.destroy();
    }
}
