package states;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.util.FlxColor;

import sprites.Player;


class WinState extends FlxState
{
    public var sky:FlxSprite;
    public var title:FlxText;
    public var start:FlxText;
    public var player:Player;
    public var controls:FlxText;

    override public function create():Void
    {
        super.create();

        FlxG.mouse.visible = false;

        sky = new FlxSprite(0, 0);
        sky.loadGraphic("assets/images/sky.png");
        add(sky);

        title = new FlxText(10, 5, 0, "You Defended\nThe Dungeon!!");
        title.color = 0xFFDD0000;
        title.borderSize = 2;
        title.borderStyle = FlxTextBorderStyle.OUTLINE;
        title.size = 12;
        title.alignment = FlxTextAlign.CENTER;
        title.x = (FlxG.width / 2) - (title.fieldWidth / 2);
        add(title);

        start = new FlxText(10, 35, 0, "Space to Continue");
        start.alignment = FlxTextAlign.CENTER;
        start.color = 0xFFebea9d;
        start.x = (FlxG.width / 2) - (start.fieldWidth / 2);
        add(start);

        player = new Player(45, 25);
        player.awakeness = 0;
        add(player);

        controls = new FlxText(10, FlxG.height - 20, 0, "Rest easy monster...");
        controls.alignment = FlxTextAlign.CENTER;
        controls.color = 0xFFc39b9b;
        controls.x = (FlxG.width / 2) - (controls.fieldWidth / 2);
        add(controls);

        FlxG.camera.fade(FlxColor.BLACK, 1, true);
    }

    override public function update(elapsed:Float):Void
    {
        super.update(elapsed);

        player.animation.play("sleep");
        player.facing = FlxObject.LEFT;
        player.velocity.x = 0;

        if (FlxG.keys.justPressed.SPACE)
        {
            FlxG.camera.fade(FlxColor.BLACK, 1, false, function () {
                FlxG.switchState(new CreditsState());
            });
        }
    }

    override public function destroy():Void
    {
        super.destroy();
    }
}
